# Quickstart

## First Steps

* Create an HTML document whose `body` includes a `header` tag, a `footer` tag
  and a `main` tag.
* Download [`printstrap.css`](https://gitlab.com/castedo/printstrap/-/blob/main/dist/printstrap.css)
  and include it as CSS in your HTML document.
* Open your HTML document in your browser.
* Add some content, following the [guidelines](guidelines.md).

## Minimal HTML Sample

```html
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="printstrap.css">
  </head>
  <body>
    <header></header>
    <footer></footer>
    <main>
      <h1>Document</h1>
    </main>
  </body>
</html>
```
