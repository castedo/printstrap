# Guidelines

## Workflow

The HTML file passed to WeasyPrint is the *source HTML file*. Firefox, Chromium etc…
are *screen web browsers*. When the *source HTML file* is opened by a *screen web
browser* it should render in *infinite paper preview* mode by default. In this mode, the
user can see the entire document printed on a single arbitrarily long piece of paper.

## Page Margins

You can place content for the top and bottom page-margin boxes in `header` and
`footer` elements, respectively. The margins are split into three different
blocks, whose proportions are set by CSS variables:
```
:root {
  --ps-page-top-left-width: 35%;
  --ps-page-top-center-width: 30%;
  --ps-page-top-right-width: 35%;
  --ps-page-bottom-left-width: 35%;
  --ps-page-bottom-center-width: 30%;
  --ps-page-bottom-right-width: 35%;
}
```
and can be overridden. Only use `%` units.

You can define the content to be displayed using `div`s with the `page-left`,
`page-center` and `page-right` classes:
```
<header>
  <div class="page-left">Top left</div>
  <div class="page-center">Top center</div>
  <div class="page-right">Top right</div>
</header>
<footer>
  <div class="page-left">Bottom left</div>
  <div class="page-center">Bottom center</div>
  <div class="page-right">Bottom right</div>
</footer>
```

When open in a *screen web browser*, the headers and footers will be displayed
once, respectively at the top and at the bottom of the *infinite paper
preview*. On the printed document, the headers and footers are repeated on each
page.

It is possible to override the content of the PDF headers and footers, for
example to include page numbers, by setting the page margin content in CSS. For
example:

```css
footer .page-right {
  content: counter(page) " of " counter(pages);
}
```

## Best Practice

* Do not style the `html` or `body` elements in the source HTML file. Instead, set the root CSS
  variables available in
  [`printstrap.css`](https://gitlab.com/castedo/printstrap/-/blob/main/dist/printstrap.css).
  Let Printstrap do all styling of the `html` and `body` elements in the source HTML file.
* Do not style directly under the `@page` selector.
  Instead, override CSS variables of printstrap.
* Try to use CSS features that are supported by WeasyPrint, in order to get
  consistent screen and print renderings. The main CSS features that WeasyPrint
  may not handle well are Grid and Flex layouts. You can also read
  [an exhaustive list in WeasyPrint’s documentation](https://doc.courtbouillon.org/weasyprint/stable/api_reference.html#css).
