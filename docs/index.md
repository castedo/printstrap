# Printstrap

Printstrap is a toolkit for taking HTML content of a document/article
and rendering it into both a PDF file and a web page.
This dual-rendering is useful for both:

1. fast preview and debugging of how HTML will look in a PDF file (approximately) and
2. making a document/article available as both a PDF file and an online web page.

Printstrap is engineered to work with WeasyPrint, but may work with other
web-to-PDF generators. This toolkit consists of
[a CSS stylesheet](https://gitlab.com/castedo/printstrap/-/blob/main/dist/printstrap.css)
that can be included in the HTML file passed to WeasyPrint.

## Table of Contents

- [Quickstart](quickstart.md)
- [Guidelines](guidelines.md)
- [Examples](examples.md)
